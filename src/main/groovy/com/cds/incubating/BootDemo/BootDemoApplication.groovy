package com.cds.incubating.BootDemo

import groovy.transform.CompileStatic

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.EnableWebMvc

import com.cds.groovy.domain.global.customer.Customer
import com.cds.groovy.domain.global.customer.GlobalCustomer
import com.cds.groovy.domain.global.customer.GlobalCustomerResponse
import com.cds.groovy.domain.global.customer.Order
import com.cds.groovy.domain.recreg.Address
import com.cds.groovy.domain.recreg.Assignment
import com.cds.groovy.domain.recreg.AssignmentDates
import com.cds.groovy.domain.recreg.Email
import com.cds.groovy.domain.recreg.Product
import com.cds.groovy.domain.recreg.RecRegResponse
import com.cds.groovy.domain.recreg.User
import com.cds.groovy.service.GlobalCustomerLookupService
import com.cds.groovy.service.RecRegLookupService
import com.cds.groovy.web.utils.GetUrl
import com.cds.groovy.web.utils.GetUrlIF
import com.cds.groovy.xstream.GlobalCustomerXstream
import com.cds.groovy.xstream.RecRegXstream
import com.thoughtworks.xstream.XStream
import com.thoughtworks.xstream.io.xml.StaxDriver

@SpringBootApplication
@Configuration
@CompileStatic
@EnableWebMvc
@ComponentScan(basePackages="com.cds")
class BootDemoApplication {

	static void main(String[] args) {
		SpringApplication.run BootDemoApplication, args
	}
	
	@Bean
	public RecRegLookupService recRegService() {
		return new RecRegLookupService();
	}
	
	@Bean
	public GlobalCustomerLookupService globalCustomerLookupService() {
		return new GlobalCustomerLookupService();
	}
	
	@Bean
	public GlobalCustomerXstream getGlobalCustomerXstream() {
		return new GlobalCustomerXstream()
	}
	
	@Bean
	public RecRegXstream getRecRegXstream() {
		return new RecRegXstream()
	}
	
	@Bean
	@Qualifier("globalCustomerXstream")
	public XStream globalCustomerXstream () {
		XStream xstream = configureForRecRegResponse(new XStream(new StaxDriver()))
		xstream.alias("transaction", GlobalCustomerResponse.class)
		xstream.addImplicitCollection(GlobalCustomer.class, "customers", "customer", Customer.class);
		xstream.addImplicitCollection(Customer.class, "orders", "order", Order.class)
		xstream.ignoreUnknownElements();
		return xstream
	}
	
	@Bean
	@Qualifier("recRegXstream")
	public XStream recRegXstream() {
		XStream recRegXstream = configureForRecRegResponse(new XStream(new StaxDriver()))
		recRegXstream.ignoreUnknownElements();
		return recRegXstream
	}
	
	@Bean
	public GetUrlIF getUrl() {
		return new GetUrl()
	}
	
	private XStream configureForRecRegResponse(XStream xstream) {
		xstream.alias("address", Address.class)
		xstream.alias("assignment", Assignment.class)
		xstream.alias("assignmentDates", AssignmentDates.class)
		xstream.alias("email", Email.class)
		xstream.alias("product", Product.class)
		xstream.alias("response", RecRegResponse.class)
		xstream.alias("user", User.class)
		
		xstream.aliasField("products", User.class, "productList")
		xstream.aliasField("addresses", User.class, "addressList")
		xstream.aliasField("data", RecRegResponse.class, "userList")
		xstream.aliasField("assignments", Address.class, "assignmentList")
		xstream.aliasField("emails", User.class, "emailList")
		xstream.aliasField("assignments", Email.class, "assignmentList")
		xstream.aliasField("assignmentDates", Assignment.class, "dateList")
		return xstream
	}
}
