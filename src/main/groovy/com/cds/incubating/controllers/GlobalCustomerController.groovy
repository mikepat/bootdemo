package com.cds.incubating.controllers

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

import com.cds.groovy.domain.global.customer.GlobalCustomerResponse
import com.cds.groovy.service.GlobalCustomerLookupService

@RestController
class GlobalCustomerController {
	
	@Autowired
	GlobalCustomerLookupService service
	
	@RequestMapping(path = "/passthru/{mag}/{acct}/{zip}", method = RequestMethod.GET, produces = "application/json")
	public GlobalCustomerResponse passThru(@PathVariable String acct, @PathVariable String mag, @PathVariable String zip) {
		GlobalCustomerResponse response = service.doLookupByAcct(acct, mag, zip)
		
		return response;
	}
}
